# Lightweight Service Description Language (LSDL)

## Format

Das erwartete Format ist eine UTF-8 kodierte JSON-Textdatei.

## Elemente

Die Wurzel kann die nachfolgend dargestellten Elemente beinhalten. Sie werden in der angegebenen Reihenfolge verarbeitet, können aber in beliebiger Reihenfolge in der Beschreibungsdatei vorliegen.

* prefix
* vendor
* domains
* relations
* views
* lists
* forms
* pages
* services

### Prefix

Das Präfix wird den erzeugten Klassen- und Tabellennamen vorangestellt.

#### Beispiel

`"prefix": "meinportal"` erzeugt etwa eine Klasse: `class MeinportalController {...}` oder eine Tabelle: `meinportal_user`

### Vendor

Die Vendorkennung dient als Namespace für den Code.

#### Beispiel

`"prefix": "meinefirma"` erzeugt etwa den Namespace: `namespace meinefirma\server\meinportal`

### Domains

`domains` ist ein Objekt, welches die Definitionen für die Datentabellen beinhaltet. Jeder Schlüssel im Objekt dient als Tabellenname und muss eindeutig sein. Gültige Zeichen sind `[0-9a-zA-Z]`. Jede Domain definiert die einzelnen Datenfelder (`text_fields`, `geo_fields`, `integer_fields`, `real_fields`, `confirm_fields`, `date_fields`), die Vorschaufelder `preview_fields`) und die zulässigen Methoden (`methods`).

#### Datenfelder

* text_fields: volltext-indizierbare Textfelder
* geo_fields: volltext-indizierbare Textfelder der Ausprägungen `street`, `zip`, `city` und/oder `country`, die über einen Dienst auch eine Umkreissuche ermöglichen
* integer_fields: filterbare Ganzzahlfelder
* real_fields: filterbare Fließkommafelder
* confirm_fields: filterbare Flags, die auch den Zeitstempel des Setzen des Flags mit speichern
* date_fields: filterbare Datumsfelder

Neben den gültigen Zeichen für Tabellennamen ist auch `_` (underscore) zulässig für Feldbezeichner.

Hinweis: Automatisch wird immer das Feld `id` (Primärschlüssel) sowie `last_mod` (Zeitstempel der letzten Modifikation) erzeugt.

#### Vorschaufelder

Dies ist eine Liste mit denjenigen Feldern, die bei einer `list`-Anfrage mit ausgeliefert werden sollen. Fehlt diese Angabe, wird nur die `id` der Einträge zurückgeliefert.

Beispiel: `"preview_fields": ["id", "name"]`

#### Methoden

Die zulässigen Methoden werden über eine Zeichenkette mit den Buchstaben c (für Create), r (für Read), u (für Update) und/oder d (für Delete) angegeben. Um Upsert zu ermöglichen, müssen sowohl c, als auch u angegeben werden. Werden keine Methoden angegeben, ist nur `list` und `filter` zulässig. Zulässigkeit bedeutet, dass die Methoden innerhalb der Services verwendet werden können. Ohne definierten Service ist ein Aufruf der Methoden nicht möglich.

Beispiel: `"methods": "crud"`

##### Beispiel

    "domains": {
      "person": {
        "text_fields": ["vorname", "nachname"],
        "integer_fields": ["alter"],
        "preview_fields": ["id", "nachname"],
        "methods": "crud"
      }
    }

In diesem Beispiel wird ein Schema für Personenangaben definiert, welches Vorname, Nachname und Alter enthalten würde.

### Relations

`relations` ist ein ähnlich aufgebautes Objekt wie `domains`, insbesondere können die Daten- und Vorschaufelder sowie Methoden auf die gleiche Weise definiert werden. Zusätzlich gibt das Attribut `type` die Art der Relation an (1:1, 1:n, n:1 oder n:m).

Der Bezeichner muss zwingend die beiden zu verbindenden Domänen mit einem `_` (underscore) getrennt enthalten. Daraus wird für jede Domäne auch ein Fremdschlüsselfeld nach dem Schema `domain_id` erzeugt. Für die Fremdschlüssel wird automatisch Kaskadierung als Lösungsweg für UPDATE oder DELETE aktiviert.

##### Beispiel

    "relations": {
      "user_person": {
        "type": "1:1",
        "integer_fields": ["state"],
        "preview_fields": ["id", "user_id", "person_id"],
        "methods": "crud"
      }
    }

Dieses Beispiel würde es erlauben jedem Nutzer einen Datensatz für Personenangaben zuzuordnen, dabei kann auch ein Status (`state`) der Beziehung gespeichert werden (etwa ob die Personenangaben vollständig sind).

### Views

`views` erzeugt eine SQL VIEW und wird über eine Liste der relevanten Domänen (`domains`), des JOIN-Typs (`join_type`) und der Vorschaufelder (`preview_fields`) definiert. JOIN-Typ kann `LEFT`, `RIGHT`, `INNER` oder `CROSS` sein. Wird kein Typ angegeben, wird automatisch `CROSS` (Kartesisches Produkt) verwendet.

Wird nicht das kartesische Produkt als JOIN-Typ verwendet, muss dem Domänennamen auch die Bedingung mittels SQL-Notation (`ON (...)`) folgen. Innerhalb der Bedingung muss den Tabellennamen ein `§` vorangestellt werden um automatisch das Präfix ergänzen zu lassen.

Es können sowohl Tabellen aus `domains`, als auch aus `relations` verwendet werden. Die Hauptdomäne muss im Bezeichner der View mit einem `_` (underscore) getrennt auf der linken Seite erscheinen.

#### Beispiel

    "views": {
      "user_person_all": {
        "domains": ["user", "user_person ON (§user.id = §user_person.user_id"],
        "join_type": "LEFT",
        "preview_fields": ["user_id", "person_id"]
      }
    }

Dieses Beispiel würde alle Nutzer auflisten und - sofern verfügbar - ihre Verknüpfung zu Personendaten.

### Lists

`lists` definiert Listen, die in Hypertext-Dokumente eingebunden werden können. Jeder Schlüssel dient als Bezeichner für eine Liste, die mittels `$autolist:name_of_list` eingebunden werden kann. Gültige Zeichen für den Bezeichner sind `[0-9a-z._]`. Mögliche Attribute sind `autobind`, `service_list` und `pages`.

* autobind: die Domäne bzw. View aus der die Daten geholt werden
* service_list: der Service, welcher zum Datenholen getriggert wird
* pages: eine Liste mit Seiten, die `fields` (Felder) und `triggers` (Aktionen) definieren

#### Felder

`fields` ist ein Objekt, dessen Schlüssel als Feldnamen dienen. Die Feldnamen müssen mit den Feldern aus der angegebenen automatisch zu bindenden Domäne bzw. View übereinstimmen. Ein Label kann mittels des Attributs `label` definiert werden. Zulässig ist hier die Verwendung von Übersetzungen mittels `$translate:name_of_message`. Die Übersetzungen werden in templates/localization/*.loc.json definiert und beim Seitenaufruf abgerufen.

#### Aktionen

`triggers` ist ein Objekt, dessen Schlüssel als Bezeichner für Aktionen dienen. Angegeben werden kann ein `label` und ein in `services` definierter `service`. Dargestellt werden die Aktionen als aufrufbares Element.

#### Beispiel

    "lists": {
      "listpersondata": {
        "autobind": ["person"],
        "service_list": "personlist",
        "pages": [{
          "fields": {
            "prename": {
              "label": "Vorname"
            },
            "surname": {
              "label": "Nachname"
            }
          }
        }]
      }
    }

### Forms

`forms` definiert Formulare, die in Hypertext-Dokumente eingebunden werden können. Jeder Schlüssel dient als Bezeichner für ein Formular, das mittels `$autoform:name_of_form` eingebunden werden kann. Gültige Zeichen für den Bezeichner sind `[0-9a-z._]`. Mögliche Attribute sind `autobind`, `service_read`, `service_write` und `pages`.

* service_read: der Service, welcher zum Datenholen getriggert wird
* service_write: der Service, welcher zum Datenschreiben getriggert wird

Die anderen Attribute verhalten sich wie bei Lists. Bei den Aktionen ist zu beachten, dass bei Formularen die Spezialaktionen `submit` und `reset` vordefiniert sind und eingebunden werden können, wobei `submit` den `service_write` triggert.

#### Beispiel

    "forms": {
      "person": {
        "autobind": ["person"],
        "service_read": "person_read",
        "service_write": "person_write",
        "pages": [{
          "fields": {
            "prename": {
              "label": "Vorname"
            },
            "surname": {
              "label": "Nachname"
            }
          }
        }],
        "triggers": [{
          "submit": {
          }
        }]
      }
    }

### Pages

`pages` definiert die einzelnen Hypertext-Seiten, die der Nutzer aufrufen kann. Jeder Schlüssel dient als Bezeichner für eine Seite. Gültige Zeichen für den Bezeichner sind `[0-9a-zA-Z-._~]`.

### Services

`services` definiert die einzelnen Services, die über die API angeboten werden. Jeder Schlüssel dient als Bezeichner für einen Service. Gültige Zeichen für den Bezeichner sind `[0-9a-zA-Z-._~]`. Die Rückgabe wird im Attribut `return` beschrieben.

#### Return

Das Feld `return` dient der Spezifikation welche Methode oder Datenbankaktion ausgelöst werden soll.

Bei einer Datenbankaktion müssen die Attribute `domain` und/oder `relation` angegeben werden. Die aufzurufende Aktionsmethode wird über das Attribut `method` angegeben und die Werte `create`, `read`, `update`, `delete`, `enumerate`, `filter`, `readall` oder `upsert` annehmen.

* create: Erstellung eines neuen Datensatzes, erwartet Objekt als ersten Parameter
* read: Lese einen Datensatz, erwartet ID als ersten Parameter
* update: Schreibe in einen bestehenden Datensatz, erwartet ID als ersten und ein Objekt als zweiten Parameter
* delete: Lösche einen Datensatz, erwartet ID als ersten Parameter
* enumerate: Liste alle Datensätze der Domäne oder Relation auf
* filter: Liste gefilterte Datensätze der Domäne oder Relation auf, erwartet Filter als ersten Parameter
* readall: Lese einen Datensatz aus der Relation und gebe den verbundenen Datensatz aus der angegebenen bzw. rechtsseitigen Domäne zurück, erwartet ID der linksseitigen Domäne der Relation als ersten Parameter
* upsert: legt einen neuen Datensatz in der Relation und ggf. in der Domäne an oder schreibt in einen bestehenden Datensatz, sie unterscheidet sich hinsichtlich der Parameter je nach Relationstyp und ob eine Domäne spezifiziert ist:
  * nur Relation:
    * _1:1_: erwartet ID der linksseitigen Domäne der Relation als ersten, ID der rechtsseitige Domäne der Relation als zweiten und ein Objekt als dritten Parameter
    * _1:n_: erwartet IDs der Domänen der Relation als ersten und zweiten Parameter und ein Objekt als dritten Parameter
    * _n:m_: erwartet IDs der Domänen der Relation als ersten und zweiten Parameter und ein Objekt als dritten Parameter
  * Relation und Domäne:
    * _1:1_: erwartet ID der linksseitigen Domäne der Relation als ersten und ein Objekt als dritten Parameter
    * _1:n_: erwartet IDs der Domänen der Relation als ersten und zweiten Parameter und ein Objekt als dritten Parameter
    * _n:m_: erwartet IDs der Domänen der Relation als ersten und zweiten Parameter und ein Objekt als dritten Parameter

Das Attribut `parameters` ist eine Liste mit Parametern, die an die Methode oder Aktion in der angegebenen Reihenfolge übergeben werden. Objektpfade werden aufgelöst. Gültige Zeichen für einzelne Bezeichner sind `[0-9a-zA-Z_]`. Vordefinierte Objekte sind `user`, welches das Merkmal `id` enthält und `data`, welches in der Regel `payload` als Unterobjekt enthält. Objekte können auch als JSON kodiert sein. Filter sind Array, die entweder SQL-Filterkriterien als Zeichenketten enthalten oder die Bezeichner `or` oder `and` unter denen weitere Kriterien vorhanden sind. Die Kriterien in der Wurzel werden mit `and` verknüpft.

#### Beispiel

    "services": {
      "list_person": {
        "return": {
          "domain": "person",
          "method": "filter",
          "parameters": ["[\"surname IS NOT NULL\"]"]
        }
      }
    }
